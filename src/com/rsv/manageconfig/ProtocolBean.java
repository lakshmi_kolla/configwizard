/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rsv.manageconfig;

import org.json.JSONArray;

/**
 *
 * @author lakshmi
 */
public class ProtocolBean {
    private String clinicalTrialProtocolId;
    private String clinicalTrialProtocolName;
    private String clinicalTrialSponsorName;
    private JSONArray timepointsArray;

    /**
     * @return the clinicalTrialProtocolId
     */
    public String getClinicalTrialProtocolId() {
        return clinicalTrialProtocolId;
    }

    /**
     * @param clinicalTrialProtocolId the clinicalTrialProtocolId to set
     */
    public void setClinicalTrialProtocolId(String clinicalTrialProtocolId) {
        this.clinicalTrialProtocolId = clinicalTrialProtocolId;
    }

    /**
     * @return the clinicalTrialProtocolName
     */
    public String getClinicalTrialProtocolName() {
        return clinicalTrialProtocolName;
    }

    /**
     * @param clinicalTrialProtocolName the clinicalTrialProtocolName to set
     */
    public void setClinicalTrialProtocolName(String clinicalTrialProtocolName) {
        this.clinicalTrialProtocolName = clinicalTrialProtocolName;
    }

    /**
     * @return the clinicalTrialSponsorName
     */
    public String getClinicalTrialSponsorName() {
        return clinicalTrialSponsorName;
    }

    /**
     * @param clinicalTrialSponsorName the clinicalTrialSponsorName to set
     */
    public void setClinicalTrialSponsorName(String clinicalTrialSponsorName) {
        this.clinicalTrialSponsorName = clinicalTrialSponsorName;
    }

    /**
     * @return the timepointsArray
     */
    public JSONArray getTimepointsArray() {
        return timepointsArray;
    }

    /**
     * @param timepointsArray the timepointsArray to set
     */
    public void setTimepointsArray(JSONArray timepointsArray) {
        this.timepointsArray = timepointsArray;
    }
    
}
