/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rsv.manageconfig;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.json.JSONArray;
import org.json.JSONException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.SAXException;

/**
 *
 * @author lakshmi
 */
public class ReadDTFFile {
    public static void main(String[] args) throws IOException {
        try {
            Node dtfBackupNode = null;
            
            File file  = new File("/home/lakshmi/Desktop/DTFConfig.xml");
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder;
            dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(file);
            
            NodeList dicomNode = doc.getElementsByTagName("dicom");
            System.out.println("dicom node length is "    +dicomNode.getLength());
            System.out.println("length of dicom childnodes is " +dicomNode.item(0).getChildNodes().getLength());
            NodeList dicomDtfNode = dicomNode.item(0).getChildNodes();
            Element dicomdtf = (Element)dicomNode.item(0);
            Node dtf = dicomdtf.getElementsByTagName("dtf").item(0);
            System.out.println("dtf node is "  +dtf.getNodeName());
            dtfBackupNode = dtf.cloneNode(true);
            NodeList dtfsNodes = doc.getElementsByTagName("dtfs").item(0).getChildNodes();
            System.out.println("dtfsnodes child lenght "  +dtfsNodes.getLength());
            
            for(int i=0;i<dtfsNodes.getLength();i++){
                  if (dtfsNodes.item(i) instanceof Text) {
                    continue;
            } else {
                      System.out.println("---------------"  +dtfsNodes.item(i).getNodeName());
                      if(dtfsNodes.item(i).getNodeName() == "dicom"){
                          System.out.println(" dtfsNodes.item(i).getParentNode()    " + dtfsNodes.item(i).getParentNode());
                          //dtfsNodes.item(i).getParentNode().removeChild(dtfsNodes.item(i));
                          doc.getElementsByTagName("dtfs").item(0).removeChild(dtfsNodes.item(i));
                      }
                      //doc.removeChild(dtfsNodes.item(i).getChildNodes().item(0));
                      //doc.removeChild(dtfsNodes.item(i));
            }
            }
            JSONArray timepointDesArray = new JSONArray();
            timepointDesArray.put("V1");
            timepointDesArray.put("V2");
            Element dicomElement = doc.createElement("dicom");
            
            //doc.getElementsByTagName("dtfs").item(0).appendChild(dicomElement);
            for(int t=0;t<timepointDesArray.length();t++){
              Node dtfNode = dtfBackupNode.cloneNode(true);
              dtfNode.getAttributes().item(0).setNodeValue(timepointDesArray.getString(t));
              dicomElement.appendChild(dtfNode);             
              //doc.getElementsByTagName("dtfs").item(0).appendChild(dicomElement);
            }
          
            doc.getElementsByTagName("dtfs").item(0).appendChild(dicomElement);
                   // .appendChild(dicomElement);
            
            
           /* Element dtfElement = null;
            
            //System.out.println("-----------------" +dtfBackupNode.getAttribute("name"));
            
            for(int i=0;i<dicomDtfNode.getLength();i++){
                
               if (dicomDtfNode.item(i) instanceof Text) {
                    continue;
            } else {
                dtfElement= (Element)dicomDtfNode.item(i);
                   System.out.println("   fffffff       " +dtfElement.getAttribute("name"));
            }
            }*/
            //dtfBackupNode = dicomDtfNode;
           // System.out.println("      " +dicomDtfNode.hasChildNodes());
        doc.getDocumentElement().normalize();
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        DOMSource source = new DOMSource(doc);
        StreamResult result = new StreamResult(file);
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.transform(source, result);
                     
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(ReadDTFFile.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SAXException ex) {
            Logger.getLogger(ReadDTFFile.class.getName()).log(Level.SEVERE, null, ex);
        } catch (TransformerConfigurationException ex) {
            Logger.getLogger(ReadDTFFile.class.getName()).log(Level.SEVERE, null, ex);
        } catch (TransformerException ex) {
            Logger.getLogger(ReadDTFFile.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JSONException ex) {
            Logger.getLogger(ReadDTFFile.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
