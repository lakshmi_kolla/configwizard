/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rsv.manageconfig;

import org.json.JSONObject;

/**
 *
 * @author lakshmi
 */
public class ImportConfigPropertiesBean {
    private String notificationqueryfields;
    private String notificationto;
    private String modifydcm;
    private String CreateTask;
    private String nondicomsupports;
    private String NonDicomCreateTask;
    private JSONObject importConfigObj;

    /**
     * @return the notificationqueryfields
     */
    public String getNotificationqueryfields() {
        return notificationqueryfields;
    }

    /**
     * @param notificationqueryfields the notificationqueryfields to set
     */
    public void setNotificationqueryfields(String notificationqueryfields) {
        this.notificationqueryfields = notificationqueryfields;
    }

    /**
     * @return the notificationto
     */
    public String getNotificationto() {
        return notificationto;
    }

    /**
     * @param notificationto the notificationto to set
     */
    public void setNotificationto(String notificationto) {
        this.notificationto = notificationto;
    }

    /**
     * @return the modifydcm
     */
    public String getModifydcm() {
        return modifydcm;
    }

    /**
     * @param modifydcm the modifydcm to set
     */
    public void setModifydcm(String modifydcm) {
        this.modifydcm = modifydcm;
    }

    /**
     * @return the CreateTask
     */
    public String getCreateTask() {
        return CreateTask;
    }

    /**
     * @param CreateTask the CreateTask to set
     */
    public void setCreateTask(String CreateTask) {
        this.CreateTask = CreateTask;
    }

    /**
     * @return the nondicomsupports
     */
    public String getNondicomsupports() {
        return nondicomsupports;
    }

    /**
     * @param nondicomsupports the nondicomsupports to set
     */
    public void setNondicomsupports(String nondicomsupports) {
        this.nondicomsupports = nondicomsupports;
    }

    /**
     * @return the NonDicomCreateTask
     */
    public String getNonDicomCreateTask() {
        return NonDicomCreateTask;
    }

    /**
     * @param NonDicomCreateTask the NonDicomCreateTask to set
     */
    public void setNonDicomCreateTask(String NonDicomCreateTask) {
        this.NonDicomCreateTask = NonDicomCreateTask;
    }

    /**
     * @return the importConfigObj
     */
    public JSONObject getImportConfigObj() {
        return importConfigObj;
    }

    /**
     * @param importConfigObj the importConfigObj to set
     */
    public void setImportConfigObj(JSONObject importConfigObj) {
        this.importConfigObj = importConfigObj;
    }
    
}
