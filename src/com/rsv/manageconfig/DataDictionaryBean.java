/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rsv.manageconfig;

import java.util.Set;
import org.json.JSONArray;

/**
 *
 * @author lakshmi
 */
public class DataDictionaryBean {    
    private Set<String> modalitiesArray;
    private JSONArray timepointidsArray;
    private JSONArray timepointdesArray;
    private JSONArray seriesDesArray;

    /**
     * @return the modalitiesArray
     */
    public Set getModalitiesArray() {
        return modalitiesArray;
    }

    /**
     * @param modalitiesArray the modalitiesArray to set
     */
    public void setModalitiesArray(Set modalitiesArray) {
        this.modalitiesArray = modalitiesArray;
    }

    /**
     * @return the timepointidsArray
     */
    public JSONArray getTimepointidsArray() {
        return timepointidsArray;
    }

    /**
     * @param timepointidsArray the timepointidsArray to set
     */
    public void setTimepointidsArray(JSONArray timepointidsArray) {
        this.timepointidsArray = timepointidsArray;
    }

    /**
     * @return the timepointdesArray
     */
    public JSONArray getTimepointdesArray() {
        return timepointdesArray;
    }

    /**
     * @param timepointdesArray the timepointdesArray to set
     */
    public void setTimepointdesArray(JSONArray timepointdesArray) {
        this.timepointdesArray = timepointdesArray;
    }

    /**
     * @return the seriesDesArray
     */
    public JSONArray getSeriesDesArray() {
        return seriesDesArray;
    }

    /**
     * @param seriesDesArray the seriesDesArray to set
     */
    public void setSeriesDesArray(JSONArray seriesDesArray) {
        this.seriesDesArray = seriesDesArray;
    }
    
    
}
