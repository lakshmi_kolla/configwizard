/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rsv.manageconfig;

import org.json.JSONArray;

/**
 *
 * @author lakshmi
 */
public class FolderStructBean {
    private JSONArray foldernameArray;

    /**
     * @return the foldernameArray
     */
    public JSONArray getFoldernameArray() {
        return foldernameArray;
    }

    /**
     * @param foldernameArray the foldernameArray to set
     */
    public void setFoldernameArray(JSONArray foldernameArray) {
        this.foldernameArray = foldernameArray;
    }
    
}
