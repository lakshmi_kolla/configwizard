/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rsv.manageconfig;

import com.jnj.ice.jhove.JhoveModel;
import com.mis.jms.JMSUtils;
//import com.mis.trialfolderwizard.CopyConfigFolderAction;
import com.mis.utils.Utils;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.extensions.webscripts.AbstractWebScript;
import org.springframework.extensions.webscripts.WebScriptRequest;
import org.springframework.extensions.webscripts.WebScriptResponse;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.SAXException;
import org.alfresco.error.AlfrescoRuntimeException;
import org.alfresco.model.ContentModel;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.action.Action;
import org.alfresco.service.cmr.repository.ContentData;
import org.alfresco.service.cmr.repository.ContentService;
import org.alfresco.service.cmr.repository.ContentWriter;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.namespace.QName;
import org.alfresco.web.bean.repository.Repository;
import org.springframework.jms.core.JmsTemplate;

/**
 *
 * @author lakshmi
 */
public class DatadictionaryXmlFileActions extends AbstractWebScript {

    private ServiceRegistry serviceRegistry;

    private static final Log logger = LogFactory.getLog(DatadictionaryXmlFileActions.class);
    private final DataDictionaryBean dataDictionaryBean = new DataDictionaryBean();
    private ProtocolBean protocoBean = new ProtocolBean();
    private FolderStructBean folderStructBean = new FolderStructBean();
    private ImportConfigPropertiesBean importconfigBean = new ImportConfigPropertiesBean();
    private List<File> fileNamesSet = new ArrayList<File>();
    private Set<String> fileList = new HashSet<String>();
 
    //String errorMsg = "";
    //String success = "";

    private static final QName TYPE_CTSPACE = QName.createQName("http://com.mis.org/model/clinicaltrial/1.0",
            "CTSpace");
    private static final QName COMPOUND_SPACE = QName.createQName(
            "{http://com.mis.org/model/compoundtrial/1.0}CompoundSpace");
    private JmsTemplate jmsTemplate;
    JSONObject resultObj = new JSONObject();

    public void listFilesForFolder(File folder, String jsonString) throws IOException, XPathExpressionException, TransformerException, ParserConfigurationException, SAXException, JSONException {
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        dbFactory.setIgnoringElementContentWhitespace(true);
        DocumentBuilder dBuilder;
        dBuilder = dbFactory.newDocumentBuilder();
        
        Document doc = null;
        for (File fileEntry : folder.listFiles()) {
            if (fileEntry.isDirectory()) {
                listFilesForFolder(fileEntry, jsonString);
            } else {

                if (fileEntry.getName().equalsIgnoreCase("data-dictionary.xml")) {
                    doc = dBuilder.parse(fileEntry);
                    parseDataDictionaryJsonString(jsonString);
                    dataDictionaryXmlEditActionMethod(doc);
                    writeContentToXml(doc, fileEntry);
                } else if (fileEntry.getName().equalsIgnoreCase("protocol.xml")) {
                    doc = dBuilder.parse(fileEntry);
                    parseDataDictionaryJsonString(jsonString);
                    parseProtocolJsonString(jsonString);
                    protocolXmlEditActionMethod(doc);
                    writeContentToXml(doc, fileEntry);
                } else if (fileEntry.getName().equalsIgnoreCase("FolderStruct.xml")) {
                    doc = dBuilder.parse(fileEntry);
                    parseFolderStructJsonString(jsonString);
                    folderStructXmlEditActionMethod(doc, fileEntry, jsonString);
                } else if (fileEntry.getName().equalsIgnoreCase("import_config.properties")) {
                    parseImportConfigJsonString(jsonString);
                    importConfigPropertiesEditActionMethod(fileEntry);
                }
                else if (fileEntry.getName().equalsIgnoreCase("DTFConfig.xml")) {
                   doc = dBuilder.parse(fileEntry);
                   readDTFFile(fileEntry,doc,jsonString);
                }
            }
        }
    }

    public void writeContentToXml(Document doc, File xmlFile) throws TransformerConfigurationException, TransformerException {

        doc.getDocumentElement().normalize();
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        DOMSource source = new DOMSource(doc);
        StreamResult result = new StreamResult(xmlFile);
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.transform(source, result);
    }

    /**
     * Traverse a directory and get all files, and add the file into fileList
     *
     * @param node file or directory
     */
    public void generateFileList(File node) {
        //add file only
        fileList.add(node.getAbsoluteFile().toString());
        if (node.isFile()) {
        } else if (node.isDirectory()) {
            String[] subNote = node.list();
            for (String filename : subNote) {
                generateFileList(new File(node, filename));
            }
        }
    }

    private void dataDictionaryXmlEditActionMethod(Document doc) throws JSONException {
        //try {
        NodeList propertyNodelist = doc.getElementsByTagName("property");
        Element property = null;
        Set<String> modalitySet = dataDictionaryBean.getModalitiesArray();
        JSONArray timepointDesArray = dataDictionaryBean.getTimepointdesArray();
        JSONArray timepointIdArray = dataDictionaryBean.getTimepointidsArray();
        JSONArray seriesDesArray = dataDictionaryBean.getSeriesDesArray();
        for (int i = 0; i < propertyNodelist.getLength(); i++) {
            property = (Element) propertyNodelist.item(i);
            Node name = property.getAttributeNode("name");
            if (name.getNodeValue().equalsIgnoreCase("Modality")) {
                for (int j = 0; j < property.getChildNodes().getLength(); j++) {
                    Node allowedNode = property.getChildNodes().item(j);
                    if (allowedNode.hasChildNodes()) {
                        Node valueNode = null;
                        for (int k = 0; k < allowedNode.getChildNodes().getLength(); k++) {
                            valueNode = allowedNode.getChildNodes().item(k);
                            if (valueNode.getNodeName().equalsIgnoreCase("value")) {
                                allowedNode.removeChild(valueNode);
                            }
                        }
                        for (String modality : modalitySet) {
                            Element valueElement = doc.createElement("value");
                            valueElement.appendChild(doc.createTextNode(modality));
                            allowedNode.appendChild(valueElement);
                        }
                    }
                }
            } else if (name.getNodeValue().equalsIgnoreCase("ClinicalTrialTimePointDescription")) {
                for (int j = 0; j < property.getChildNodes().getLength(); j++) {
                    Node allowedNode = property.getChildNodes().item(j);
                    if (allowedNode.hasChildNodes()) {
                        Node valueNode = null;
                        for (int k = 0; k < allowedNode.getChildNodes().getLength(); k++) {
                            valueNode = allowedNode.getChildNodes().item(k);
                            if (valueNode.getNodeName().equalsIgnoreCase("value")) {
                                allowedNode.removeChild(valueNode);
                            }
                        }
                        for (int m = 0; m < timepointDesArray.length(); m++) {
                            Element valueElement = doc.createElement("value");
                            valueElement.appendChild(doc.createTextNode(timepointDesArray.getString(m)));
                            allowedNode.appendChild(valueElement);
                        }
                    }
                }
            } else if (name.getNodeValue().equalsIgnoreCase("ClinicalTrialTimePointID")) {
                //System.out.println("------" + property.getChildNodes().getLength());                
                for (int j = 0; j < property.getChildNodes().getLength(); j++) {
                    Node allowedNode = property.getChildNodes().item(j);
                    if (allowedNode.hasChildNodes()) {
                        Node valueNode = null;
                        for (int k = 0; k < allowedNode.getChildNodes().getLength(); k++) {
                            valueNode = allowedNode.getChildNodes().item(k);
                            if (valueNode.getNodeName().equalsIgnoreCase("value")) {
                                allowedNode.removeChild(valueNode);
                            }
                        }
                        for (int m = 0; m < timepointIdArray.length(); m++) {
                            Element valueElement = doc.createElement("value");
                            valueElement.appendChild(doc.createTextNode(timepointIdArray.getString(m)));
                            allowedNode.appendChild(valueElement);
                        }
                    }
                }
            } else if (name.getNodeValue().equalsIgnoreCase("SeriesDescription")) {
                for (int j = 0; j < property.getChildNodes().getLength(); j++) {
                    Node allowedNode = property.getChildNodes().item(j);
                    if (allowedNode.hasChildNodes()) {
                        Node valueNode = null;
                        for (int k = 0; k < allowedNode.getChildNodes().getLength(); k++) {
                            valueNode = allowedNode.getChildNodes().item(k);
                            if (valueNode.getNodeName().equalsIgnoreCase("value")) {
                                allowedNode.removeChild(valueNode);
                            }
                        }
                        for (int m = 0; m < seriesDesArray.length(); m++) {
                            Element valueElement = doc.createElement("value");
                            valueElement.appendChild(doc.createTextNode(seriesDesArray.getString(m)));
                            allowedNode.appendChild(valueElement);
                        }
                    }
                }
            }
        }
    }

    private void protocolXmlEditActionMethod(Document doc) throws JSONException {
        // try {
        NodeList annogroupsTags = doc.getElementsByTagName("annon-group");
        NodeList protocolTags = doc.getElementsByTagName("protocol");
        Element tag = null;
        for (int i = 0; i < annogroupsTags.getLength(); i++) {
            NodeList tagNodeList = annogroupsTags.item(i).getChildNodes();
            Element tagElement = null;
            for (int j = 0; j < tagNodeList.getLength(); j++) {
                if (tagNodeList.item(j).getNodeName().equalsIgnoreCase("tag")) {
                    tagElement = (Element) tagNodeList.item(j);
                    String query = tagElement.getAttributeNode("query").getNodeValue();
                    if (query.contains("ClinicalTrialProtocolName")) {
                        query = query.replace(query.substring(query.indexOf("=") + 1), "\'" + protocoBean.getClinicalTrialProtocolName() + "\'" + " where PRIMARYCONDITION");
                        tagElement.getAttributeNode("query").setNodeValue(query);
                    } else if (query.contains("ClinicalTrialSponsorName")) {
                        query = query.replace(query.substring(query.indexOf("=") + 1), "\'" + protocoBean.getClinicalTrialSponsorName() + "\'" + " where PRIMARYCONDITION");
                        tagElement.getAttributeNode("query").setNodeValue(query);
                    } else if (query.contains("ClinicalTrialProtocolID")) {
                        query = query.replace(query.substring(query.indexOf("=") + 1), "\'" + protocoBean.getClinicalTrialProtocolId() + "\'" + " where PRIMARYCONDITION");
                        tagElement.getAttributeNode("query").setNodeValue(query);
                    }
                }
            }
        }

        for (int j = 0; j < protocolTags.getLength(); j++) {
            NodeList studyDescriptionsList = protocolTags.item(j).getChildNodes();
            if (studyDescriptionsList.item(1).getNodeName().equalsIgnoreCase("StudyDescriptionsList")) {
                for (int t = 0; t < studyDescriptionsList.item(1).getChildNodes().getLength(); t++) {
                    if (studyDescriptionsList.item(1).getChildNodes().item(t).getNodeName().equalsIgnoreCase("modality")) {
                        studyDescriptionsList.item(1).removeChild(studyDescriptionsList.item(1).getChildNodes().item(t));
                    }
                }
                for (Object modality : dataDictionaryBean.getModalitiesArray()) {
                    Element modalityElement = doc.createElement("modality");
                    modalityElement.setAttribute("type", modality.toString());
                    studyDescriptionsList.item(1).appendChild(modalityElement);
                }

            }

            for (int k = 0; k < studyDescriptionsList.getLength(); k++) {
                Element timepointElement = null;
                if (studyDescriptionsList.item(k).getNodeName().equalsIgnoreCase("timepoint")) {
                    timepointElement = (Element) studyDescriptionsList.item(k);
                    timepointElement.getParentNode().removeChild(studyDescriptionsList.item(k));
                }
            }
            Element timepointElement = null;
            for (int p = 0; p < protocoBean.getTimepointsArray().length(); p++) {
                JSONObject timepointObj = protocoBean.getTimepointsArray().getJSONObject(p);
                timepointElement = doc.createElement("timepoint");
                timepointElement.setAttribute("timepointID", timepointObj.getString("timepointid"));
                timepointElement.setAttribute("timepointDescription", timepointObj.getString("timepointdes"));
                timepointElement.setAttribute("offset", "0");
                timepointElement.setAttribute("windowPeriod", "0");
                timepointElement.setAttribute("shareview", "true");
                Element manufactureElement = doc.createElement("manufacturer");
                manufactureElement.setAttribute("name", "GE MEDICAL SYSTEMS");
                JSONArray modalityArray = timepointObj.getJSONArray("modality");
                for (int t = 0; t < modalityArray.length(); t++) {
                    Element modalityEle = doc.createElement("modality");
                    modalityEle.setAttribute("type", modalityArray.getString(t));
                    modalityEle.setAttribute("shareview", "true");
                    manufactureElement.appendChild(modalityEle);
                }
                timepointElement.appendChild(manufactureElement);
                protocolTags.item(j).appendChild(timepointElement);
            }
        }
    }

    private void folderStructXmlEditActionMethod(Document doc, File folderstructFile, String jsonString) throws JSONException, XPathExpressionException, TransformerConfigurationException, TransformerException, IOException {
        parseProtocolJsonString(jsonString);
        JSONArray folderNamesArray = folderStructBean.getFoldernameArray();
        //try {
        Node parentEle = doc.getElementsByTagName("clinicalTrails").item(0);
        Element trailElement = null;
        if (protocoBean.getClinicalTrialProtocolName() != null) {
            trailElement = doc.createElement(protocoBean.getClinicalTrialProtocolName());
        }
        Node PropListElement = doc.getElementsByTagName("propertieslist").item(0);

        trailElement.appendChild(PropListElement);
        Element root = doc.getDocumentElement();
        NodeList folders = ((Element) root).getElementsByTagName("folder");
        Node parentnode = folders.item(0).getParentNode();

        int start = 0;
        int l = folderNamesArray.length();
        for (int i = 0; i < folders.getLength(); i++) {
            String foldername = folders.item(i).getAttributes().item(0).getNodeValue();
            if (!folderNamesArray.toString().contains(foldername) || folders.item(i) instanceof Text) {
                continue;
            } else {
                Node folder = folders.item(i);
                parentnode.appendChild(folder);
                parentnode = folder;
                trailElement.appendChild(parentnode);
            }
        }
        parentEle.appendChild(trailElement);
        NodeList fols = ((Element) root).getElementsByTagName("folder");
        for (int i = 0; i < folders.getLength(); i++) {
            String foldername = folders.item(i).getAttributes().item(0).getNodeValue();
            if (!folderNamesArray.toString().contains(foldername) || folders.item(i) instanceof Text) {
                Node p = folders.item(i).getParentNode();
                p.removeChild(folders.item(i));
            }
        }

        boolean firstremove = true;
        for (int k = 0; k < doc.getElementsByTagName("clinicalTrails").item(0).getChildNodes().getLength(); k++) {
            Node child = doc.getElementsByTagName("clinicalTrails").item(0).getChildNodes().item(k);
            if (firstremove && child.getNodeType() == Node.ELEMENT_NODE) {
                doc.getElementsByTagName("clinicalTrails").item(0).removeChild(child);
                firstremove = false;
            }
        }
        for (int c = 0; c < trailElement.getElementsByTagName("folder").getLength(); c++) {
            XPath xPath = XPathFactory.newInstance().newXPath();
            XPathExpression xExpress = xPath.compile("//*[@Level]");
            NodeList nl = (NodeList) xExpress.evaluate(trailElement.getElementsByTagName("folder").item(c), XPathConstants.NODESET);
            nl.item(c).getAttributes().item(1).setNodeValue(Integer.toString(l));
            l--;
        }
        Writer out = new StringWriter();
        Transformer tf = TransformerFactory.newInstance().newTransformer();
        tf.transform(new DOMSource(doc), new StreamResult(out));
        FileWriter fw = new FileWriter(folderstructFile.getAbsoluteFile());
        BufferedWriter out1 = new BufferedWriter(fw);
        out1.write(out.toString());
        out1.flush();
        out1.close();
        //System.out.println(out.toString());
    }

    public Element getFirstChildElement(Node node) {
        node = node.getFirstChild();
        while (node != null && node.getNodeType() != Node.ELEMENT_NODE) {
            node = node.getNextSibling();
        }
        return (Element) node;
    }

    private void importConfigPropertiesEditActionMethod(File importConfigFile) {
        FileInputStream configStream = null;
        try {
            configStream = new FileInputStream(importConfigFile);
            Properties props = new Properties();
            props.load(configStream);
            configStream.close();
            JSONObject importconfigObj = importconfigBean.getImportConfigObj();
            Iterator<?> keys = importconfigObj.keys();
            while (keys.hasNext()) {
                String key = (String) keys.next();
                try {
                    props.setProperty(key, importconfigObj.getString(key));
                } catch (JSONException ex) {

                }
            }

            FileOutputStream output = new FileOutputStream(importConfigFile);
            props.store(output, "This description goes to the header of a file");
            output.close();
        } catch (FileNotFoundException ex) {
            // Logger.getLogger(DatadictionaryXmlFileActions.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
            // Logger.getLogger(DatadictionaryXmlFileActions.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                configStream.close();
            } catch (IOException ex) {
                ex.printStackTrace();
                //Logger.getLogger(DatadictionaryXmlFileActions.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private void parseDataDictionaryJsonString(String jsonString) throws JSONException {
        final JSONObject obj = new JSONObject(jsonString);
        Set<String> datadictionaryModalities = new HashSet<String>();
        JSONObject datadictonaryObject = obj.getJSONObject("DataDictionary");
        JSONArray modalityArray = datadictonaryObject.getJSONArray("modalities");
        JSONArray timepointIDsArray = datadictonaryObject.getJSONArray("timepointids");
        //System.out.println("timepointids length is ----    " +timepointIDsArray.length());
        JSONArray timepointDesArray = datadictonaryObject.getJSONArray("timepointdes");
        JSONArray seriesDesArray = datadictonaryObject.getJSONArray("seriesdes");
        for (int m = 0; m < modalityArray.length(); m++) {
            datadictionaryModalities.add(modalityArray.getString(m));
        }
        dataDictionaryBean.setModalitiesArray(datadictionaryModalities);
        dataDictionaryBean.setTimepointidsArray(timepointIDsArray);
        dataDictionaryBean.setTimepointdesArray(timepointDesArray);
        dataDictionaryBean.setSeriesDesArray(seriesDesArray);
    }

    private void parseProtocolJsonString(String jsonString) throws JSONException {

        final JSONObject obj = new JSONObject(jsonString);
        JSONObject protocolObj = new JSONObject(obj.getString("Protocol"));
        String protocolName = protocolObj.getString("clinicaltrialprotocolname");
        String protocolID = protocolObj.getString("clinicaltrialprotocolid");
        String sponsorName = protocolObj.getString("clinicaltrialsponsorname");
        JSONArray protocolTimePointArray = protocolObj.getJSONArray("timepoints");
        protocoBean.setClinicalTrialProtocolId(protocolID);
        protocoBean.setClinicalTrialProtocolName(protocolName);
        protocoBean.setClinicalTrialSponsorName(sponsorName);
        protocoBean.setTimepointsArray(protocolTimePointArray);
    }

    private void parseFolderStructJsonString(String jsonString) throws JSONException {
        List<String> folderNames = new ArrayList<String>();
        folderNames.add("ClinicalTrialSiteID");
        folderNames.add("ClinicalTrialSubjectID");
        folderNames.add("ClinicalTrialTimePointDescription");
        folderNames.add("Modality");
        folderNames.add("StudyInstanceUID");
        folderNames.add("SeriesInstanceUID");
        final JSONObject obj = new JSONObject(jsonString);
        JSONObject folderstructObj = new JSONObject(obj.getString("FolderStruct"));
        JSONArray folderNamesArray = folderstructObj.getJSONArray("names");
        folderStructBean.setFoldernameArray(folderNamesArray);
    }

    private void parseImportConfigJsonString(String jsonString) throws JSONException {
        final JSONObject obj = new JSONObject(jsonString);
        JSONObject importConfigObj = new JSONObject(obj.getString("ImportConfig"));
        String notificationqueryfields = importConfigObj.getString("notificationqueryfields");
        String notificationto = importConfigObj.getString("notificationto");
        String modifydcm = importConfigObj.getString("modifydcm");
        String createTask = importConfigObj.getString("CreateTask");
        String nondicomsupports = importConfigObj.getString("nondicomsupports");
        String NonDicomCreateTask = importConfigObj.getString("NonDicomCreateTask");
        importconfigBean.setImportConfigObj(importConfigObj);
        importconfigBean.setCreateTask(createTask);
        importconfigBean.setModifydcm(modifydcm);
        importconfigBean.setNonDicomCreateTask(NonDicomCreateTask);
        importconfigBean.setNondicomsupports(nondicomsupports);
        importconfigBean.setNotificationqueryfields(notificationqueryfields);
        importconfigBean.setNotificationto(notificationto);
    }

    private void copyFolderContent(File configFolder, File tempconfig) throws IOException {
        copyFolder(configFolder, tempconfig);
    }

    public void copyFolder(File src, File dest)
            throws IOException {

        //System.out.println(src.getAbsolutePath()+"/"+src.getName());
               // System.out.println(dest.getAbsolutePath()+"/"+dest.getName());

        if (src.isDirectory()) {
            if (!dest.exists()) {
                logger.info("destination folder not exists");
                dest.mkdirs();
            }

            //list all the directory contents
            String files[] = src.list();

            for (String file : files) {
                File srcFile = new File(src, file);
                File destFile = new File(dest, file);
                copyFolder(srcFile, destFile);
            }

        } else {
            
            if (!dest.getParentFile().exists()) {
                logger.info("destination folder not exists");
                dest.getParentFile().mkdirs();
            }
            InputStream in = new FileInputStream(src);
            OutputStream out = new FileOutputStream(dest);
            byte[] buffer = new byte[1024];
            int length;
            while ((length = in.read(buffer)) > 0) {
                out.write(buffer, 0, length);
            }
            in.close();
            out.close();
        }

    }

    public void addToZipFile(File file, ZipOutputStream zos) throws FileNotFoundException, IOException {
        FileInputStream fis = new FileInputStream(file.getAbsolutePath());
        ZipEntry zipEntry = new ZipEntry(file.getAbsolutePath());
        zos.putNextEntry(zipEntry);
        byte[] bytes = new byte[1024];
        int length;
        while ((length = fis.read(bytes)) >= 0) {
            zos.write(bytes, 0, length);
        }
    }

    @Override
    public void execute(WebScriptRequest req, WebScriptResponse res) {
        try {
            String trialName = req.getParameter("trialName");
            String jsonString = req.getParameter("jsonString");
            String zipFile = System.getProperty("java.io.tmpdir") + File.separator+trialName + File.separator + "config.zip";
            File configPath = new File(System.getProperty("catalina.base") + "/webapps/RSVFW/config");
            try {
                if (configPath != null) {
                    File tempconfig = new File(System.getProperty("java.io.tmpdir") +File.separator+ trialName + File.separator + "config");
                    copyFolderContent(configPath, tempconfig);
                    listFilesForFolder(tempconfig, jsonString);
                    generateFileList(tempconfig);
                    AppZip appZip = new AppZip(tempconfig.getAbsolutePath(), zipFile);
                    appZip.generateFileList(tempconfig);
                    appZip.zipIt(zipFile);
                    NodeRef rootNode = Repository.getCompanyRoot(getServiceRegistry().getNodeService(), getServiceRegistry().getSearchService(),
                            getServiceRegistry().getNamespaceService(), Repository.getStoreRef(), "app:company_home");
                    NodeRef userhomeNode = getServiceRegistry().getNodeService().getChildByName(rootNode,
                            ContentModel.ASSOC_CONTAINS, "User Homes");
                    String sponsorName = req.getParameter("sponsorname");
                    NodeRef sponsorNodeRef = getServiceRegistry().getNodeService().getChildByName(userhomeNode, ContentModel.ASSOC_CONTAINS, sponsorName);
                    if (sponsorNodeRef == null) {
                        Map<QName, Serializable> compoundProps = new HashMap<QName, Serializable>();
                        compoundProps.put(ContentModel.PROP_NAME, sponsorName);
                        compoundProps.put(ContentModel.PROP_TITLE, sponsorName);
                        compoundProps.put(QName.createQName("{http://com.mis.org/model/compoundtrial/1.0}type"), "Compound Trial");
                        //QName sponsorQName = QName.createQName(
                        //"{http://com.mis.org/model/compoundtrial/1.0}CompoundSpace", sponsorName);
                        sponsorNodeRef = getServiceRegistry().getNodeService().createNode(userhomeNode, ContentModel.ASSOC_CONTAINS, QName.createQName(NamespaceService.CONTENT_MODEL_1_0_URI,
                                sponsorName), COMPOUND_SPACE, compoundProps).getChildRef();
                    }
                    // System.out.println("sposor name already exists");
                    NodeRef trialRef = getServiceRegistry().getNodeService().getChildByName(sponsorNodeRef, ContentModel.ASSOC_CONTAINS, trialName);
                    if (trialRef == null) {
                        Map<QName, Serializable> prop = new HashMap<QName, Serializable>();
                        prop.put(ContentModel.PROP_NAME, trialName);
                        prop.put(ContentModel.PROP_TITLE, trialName);
                        prop.put(JhoveModel.PROP_CLINICALPROTOCOLID, trialName);
                        prop.put(JhoveModel.PROP_CLINICALPROTOCOLNAME, trialName);
                        prop.put(JhoveModel.PROP_CT_SPONSOR_NAME, sponsorName);
                        //QName trialQName = QName.createQName("{http://com.mis.org/model/clinicaltrial/1.0}", trialName);
                        trialRef = getServiceRegistry().getNodeService().createNode(sponsorNodeRef,
                                ContentModel.ASSOC_CONTAINS,
                                QName.createQName(NamespaceService.CONTENT_MODEL_1_0_URI,
                                        trialName), TYPE_CTSPACE, prop).getChildRef();
                        //trialRef = getServiceRegistry().getNodeService().createNode(sponsorNodeRef, ContentModel.ASSOC_CONTAINS, trialQName, ContentModel.TYPE_FOLDER, trialProps).getChildRef();
                    }
                    System.out.println("trialName is already exists");
                    NodeRef configRef = getServiceRegistry().getNodeService().getChildByName(trialRef, ContentModel.ASSOC_CONTAINS, "config");
                    if (configRef == null) {
                        uploadConfigFolder(trialRef, new File(zipFile));
                    }

                } else {
                    resultObj.put("message", "config folder not found on server,please contact Administrator");
                    resultObj.put("success", "false");
                }

            } catch (Exception ex) {
                ex.printStackTrace();
                throw new AlfrescoRuntimeException(ex.getMessage());

            }

            resultObj.put("success", "true");
            resultObj.put("message", "config uploaded successfully into created study ");
            String jsonResponse = resultObj.toString();
            try {
                res.getWriter().write(jsonResponse);
            } catch (IOException ex) {
                ex.printStackTrace();
                throw new AlfrescoRuntimeException(ex.getMessage());

            }
        } catch (JSONException ex) {
            ex.printStackTrace();
            throw new AlfrescoRuntimeException(ex.getMessage());

        }
    }

    private void uploadConfigFolder(NodeRef trailRef, File configzipFile) throws JSONException {
        Map<QName, Serializable> properties
                = new HashMap<QName, Serializable>();
        properties.put(ContentModel.PROP_NAME, "config.zip");
        properties.put(ContentModel.PROP_TITLE, "config.zip");
        String mimetype = serviceRegistry.getMimetypeService().guessMimetype("config.zip");
        ContentData contentData = new ContentData(null,
                mimetype, 0L, "UTF-8");
        properties.put(ContentModel.PROP_CONTENT,
                contentData);
        //NodeRef newconfignodeRef = serviceRegistry.getNodeService().getChildByName(trailRef, ContentModel.ASSOC_CONTAINS, "config");
        NodeRef newconfignodeRef
                = serviceRegistry.getNodeService().createNode(trailRef,
                        ContentModel.ASSOC_CONTAINS,
                        QName.createQName(NamespaceService.CONTENT_MODEL_1_0_URI,
                                "config.zip"),
                        ContentModel.TYPE_CONTENT, properties).
                getChildRef();

        ContentWriter contentWriter = serviceRegistry.getContentService().getWriter(newconfignodeRef,
                ContentModel.PROP_CONTENT, true);
        contentWriter.putContent(configzipFile);
        Map paramsmap = new HashMap<String, Object>();
        paramsmap.put("destination", trailRef);
        paramsmap.put("encoding", "UTF-8");
        Action action = serviceRegistry.getActionService().createAction("import", paramsmap);
        serviceRegistry.getActionService().executeAction(action, newconfignodeRef);
        NodeRef conref = serviceRegistry.getNodeService().getChildByName(trailRef,
                ContentModel.ASSOC_CONTAINS, "config");
        if (conref != null) {
            NodeRef studyxmlfilenoderef = serviceRegistry.getNodeService().getChildByName(conref,
                    ContentModel.ASSOC_CONTAINS, "site.xml");
            if (studyxmlfilenoderef != null) {
                siteCreation(studyxmlfilenoderef, serviceRegistry.getNodeService(), trailRef, serviceRegistry.getContentService());
            } else {
                //throw new AlfrescoRuntimeException("Config folder not having site.xml.");
            }
        }
    }

    public void siteCreation(NodeRef sitefileref, NodeService nodeService, NodeRef trailRef, ContentService contentService) {
        try {
            InputStream filecontent = contentService.getReader(sitefileref, ContentModel.PROP_CONTENT).getContentInputStream();
            DocumentBuilderFactory docBuildFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docBuildFactory.newDocumentBuilder();
            Document doc = docBuilder.parse(filecontent);
            NodeList nodelist = doc.getElementsByTagName("site");
            for (int i = 0; i < nodelist.getLength(); i++) {
                Node node = nodelist.item(i);
                if (node.getNodeType() == node.ELEMENT_NODE) {
                    Element element = (Element) node;
                    String nodeAttribute = element.getAttribute("name");
                    NodeRef sitenoderef = nodeService.getChildByName(trailRef, ContentModel.ASSOC_CONTAINS, nodeAttribute);
                    if (sitenoderef == null) {
                        sitenoderef = serviceRegistry.getFileFolderService().create(trailRef, nodeAttribute, ContentModel.TYPE_FOLDER).getNodeRef();
                        nodeService.setProperty(sitenoderef,
                                QName.createQName("http://ice.jnj.com/model/jhove/1.0", "ClinicalTrialSiteID"), nodeAttribute);
                        try {
                            final JSONObject obj = new JSONObject();
                            Utils.getJhoveProperties(nodeService, obj, sitenoderef);
                            obj.put("created", nodeService.getProperty(sitenoderef, ContentModel.PROP_CREATED));
                            obj.put("creator", nodeService.getProperty(sitenoderef, ContentModel.PROP_CREATOR));
                            obj.put("id", sitenoderef);
                            try {
                                //Start thread to send JMS Message
                                new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        JMSUtils.sendJMSMessage(getJmsTemplate(), "Site Creation", obj);
                                    }
                                }).start();

                            } catch (Exception e) {
                                e.printStackTrace();
                                logger.error("Exception to Send JMS Message");
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            logger.error("Exception to Send JMS Message");
                        }
                    }
                }
            }
        } catch (SAXException ex) {
            Logger.getLogger(DatadictionaryXmlFileActions.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(DatadictionaryXmlFileActions.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(DatadictionaryXmlFileActions.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * @return the serviceRegistry
     */
    public ServiceRegistry getServiceRegistry() {
        return serviceRegistry;
    }

    /**
     * @param serviceRegistry the serviceRegistry to set
     */
    public void setServiceRegistry(ServiceRegistry serviceRegistry) {
        this.serviceRegistry = serviceRegistry;
    }

    /**
     * @return the jmsTemplate
     */
    public JmsTemplate getJmsTemplate() {
        return jmsTemplate;
    }

    /**
     * @param jmsTemplate the jmsTemplate to set
     */
    public void setJmsTemplate(JmsTemplate jmsTemplate) {
        this.jmsTemplate = jmsTemplate;
    }  
    private void readDTFFile(File fileEntry, Document doc,String jsonString) {
        try {
            parseDataDictionaryJsonString(jsonString);
            JSONArray timepointDesArray = dataDictionaryBean.getTimepointdesArray();
            Node dtfBackupNode = null;
            //System.out.println("dtf's length    "+doc.getElementsByTagName("dtf").getLength());
            NodeList dicomNode = doc.getElementsByTagName("dicom");
            //System.out.println("dicom node length is "    +dicomNode.getLength());
           // System.out.println("length of dicom childnodes is " +dicomNode.item(0).getChildNodes().getLength());
            NodeList dicomDtfNode = dicomNode.item(0).getChildNodes();
            Element dicomdtf = (Element)dicomNode.item(0);
            Node dtf = dicomdtf.getElementsByTagName("dtf").item(0);
           // System.out.println("dtf node is "  +dtf.getNodeName());
            dtfBackupNode = dtf.cloneNode(true);
            NodeList dtfsNodes = doc.getElementsByTagName("dtfs").item(0).getChildNodes();
           // System.out.println("dtfsnodes child lenght "  +dtfsNodes.getLength());
            
            for(int i=0;i<dtfsNodes.getLength();i++){
                  if (dtfsNodes.item(i) instanceof Text) {
                    continue;
            } else {
                      System.out.println("---------------"  +dtfsNodes.item(i).getNodeName());
                      if(dtfsNodes.item(i).getNodeName() == "dicom"){
                         // System.out.println(" dtfsNodes.item(i).getParentNode()    " + dtfsNodes.item(i).getParentNode());
                          //dtfsNodes.item(i).getParentNode().removeChild(dtfsNodes.item(i));
                          doc.getElementsByTagName("dtfs").item(0).removeChild(dtfsNodes.item(i));
                      }
                      //doc.removeChild(dtfsNodes.item(i).getChildNodes().item(0));
                      //doc.removeChild(dtfsNodes.item(i));
            }
            }
            
            Element dicomElement = doc.createElement("dicom");
            for(int t=0;t<timepointDesArray.length();t++){
              Node dtfNode = dtfBackupNode.cloneNode(true);
              dtfNode.getAttributes().item(0).setNodeValue(timepointDesArray.getString(t));
              dicomElement.appendChild(dtfNode);             
            }
          
            doc.getElementsByTagName("dtfs").item(0).appendChild(dicomElement);
            writeContentToXml(doc, fileEntry);
            
            //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        } catch (JSONException ex) {
           // Logger.getLogger(DatadictionaryXmlFileActions.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        } catch (TransformerException ex) {
            ex.printStackTrace();
           // Logger.getLogger(DatadictionaryXmlFileActions.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public class AppZip {

        List<String> fileList;
        private String OUTPUT_ZIP_FILE;
        private String SOURCE_FOLDER;

        AppZip(String source, String dest) {
            this.SOURCE_FOLDER = source;
            this.OUTPUT_ZIP_FILE = dest;
            fileList = new ArrayList<String>();
        }

        /**
         * Zip it
         *
         * @param zipFile output ZIP file location
         */
        public void zipIt(String zipFile) {

            byte[] buffer = new byte[1024];

            try {

                FileOutputStream fos = new FileOutputStream(zipFile);
                ZipOutputStream zos = new ZipOutputStream(fos);
                for (String file : this.fileList) {
                    ZipEntry ze = new ZipEntry("config" + File.separator + file);
                    zos.putNextEntry(ze);
                    FileInputStream in
                            = new FileInputStream(SOURCE_FOLDER + File.separator + file);

                    int len;
                    while ((len = in.read(buffer)) > 0) {
                        zos.write(buffer, 0, len);
                    }

                    in.close();
                }
                zos.closeEntry();
                zos.close();
                //errorMsg = "Creted Zip file successfully in " + OUTPUT_ZIP_FILE;
            } catch (IOException ex) {
                //errorMsg = "Failed to Create config.zip file";
                //success = "false";
                logger.info(ex.getMessage());
            }
        }

        /**
         * Traverse a directory and get all files, and add the file into
         * fileList
         *
         * @param node file or directory
         */
        public void generateFileList(File node) {

            //add file only
            if (node.isFile()) {
                fileList.add(generateZipEntry(node.getAbsoluteFile().toString()));
            }

            if (node.isDirectory()) {
                String[] subNote = node.list();
                for (String filename : subNote) {
                    generateFileList(new File(node, filename));
                }
            }

        }

        /**
         * Format the file path for zip
         *
         * @param file file path
         * @return Formatted file path
         */
        private String generateZipEntry(String file) {
            return file.substring(SOURCE_FOLDER.length() + 1, file.length());
        }
    }
}
